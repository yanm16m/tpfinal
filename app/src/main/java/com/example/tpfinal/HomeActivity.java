package com.example.tpfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    private final String DATABASE_NAME = "Workout";

    TableLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        this.layout = (TableLayout) findViewById(R.id.TableExercice);
        loadTable();
    }

    public void loadTable() {
        SQLiteDatabase db = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        int maxCount = 0;

        // execute SQL query
        Cursor cur;
        try {
            cur = db.rawQuery("SELECT * FROM Exercice", null);
        } catch(SQLiteException e) {
            Log.d("dbQuery", "SQL error: " + e);
            return;
        }

        if(cur.moveToFirst()) {
            do {
                TableRow row = new TableRow(this);
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);

                TextView t1 = new TextView(this);
                t1.setText("Nom :" + cur.getString(1));
                TextView t2 = new TextView(this);
                t2.setText("Type :" + cur.getString(2));
                TextView t3 = new TextView(this);
                t3.setText("Répétitions :" + cur.getInt(1));
                TextView t4 = new TextView(this);
                t4.setText("Poids :" + cur.getInt(2));

                Button btnModifier = new Button(this);
                btnModifier.setText("Modifier");
                Button btnDelete = new Button(this);
                btnDelete.setText("Supprimer");

                // on ajoute les éléments dans la colonne
                row.addView(t1,(new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,2f)));
                row.addView(t2,(new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,2f)));
                row.addView(t3,(new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,2.2f)));
                row.addView(t4,(new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,2f)));
                row.addView(btnModifier,(new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,2f)));
                row.addView(btnDelete,(new TableRow.LayoutParams(0,TableRow.LayoutParams.WRAP_CONTENT,2f)));
                layout.addView(row);
            } while(cur.moveToNext());
            cur.close();
        }

    }
}
