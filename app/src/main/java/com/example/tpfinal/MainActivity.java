package com.example.tpfinal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private final String DATABASE_NAME = "Workout";

    EditText txtUsername;
    EditText txtMdp;

    TextView lblInfo;

    Button btnLogin;
    Button btnSignIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbInit();

        this.txtUsername = (EditText) findViewById(R.id.txtUsername);
        this.txtMdp = (EditText) findViewById(R.id.txtMdp);

        this.btnLogin = (Button) findViewById(R.id.btnLogin);
        this.btnSignIn = (Button) findViewById(R.id.btnSignIn);

        this.lblInfo = (TextView) findViewById(R.id.txtInfo);

        this.btnSignIn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                inscription();
            }
        });

        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                connexion();
            }
        });
    }

    public void dbInit() {
        SQLiteDatabase db = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);

        db.execSQL("DROP TABLE IF EXISTS User");
        db.execSQL("DROP TABLE IF EXISTS Exercice");
        db.execSQL("DROP TABLE IF EXISTS UserExercice");

        db.execSQL("CREATE TABLE User (" +
                "    id        INTEGER              NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "    login     VARCHAR(255)            NOT NULL," +
                "    mdp       VARCHAR(255)         NOT NULL)");

        db.execSQL("CREATE TABLE Exercice (" +
                "    id         INTEGER              NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "    name       VARCHAR(255)            NOT NULL," +
                "    type       VARCHAR(255)            NOT NULL," +
                "    rep        INTEGER            NOT NULL," +
                "    weight     INTEGER        NOT NULL)");

        db.execSQL("CREATE TABLE UserExercice (" +
                "    id        INTEGER              NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "    id_user        INTEGER              NOT NULL," +
                "    id_exercice    INTEGER              NOT NULL)");


        ContentValues vals = new ContentValues();
        vals.put("name", "test");
        vals.put("type", "cardio");
        vals.put("rep", "12");
        vals.put("weight", "0");

        // on ajoute dans la base de données
        long res = db.insert("Exercice", null, vals);
        long res2 = db.insert("Exercice", null, vals);

    }

    public boolean inscription() {
        SQLiteDatabase db = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        String username = this.txtUsername.getText().toString();
        String password = this.txtMdp.getText().toString();

        clearInfo();

        if (!(username.length() > 3 && username.length() < 18)) {
            ajouterInfo("La longueur du pseudo est trop courte ou trop grande.");
            return false;
        }

        if ((password.length() < 3)) {
            ajouterInfo("La longueur du mot de passe est trop courte.");
            return false;
        }

        ContentValues vals = new ContentValues();
        vals.put("login", username);
        vals.put("mdp", password);

        // on ajoute dans la base de données
        long res = db.insert("User", null, vals);

        ajouterInfo("Compté crée!");
        return true;
    }

    public boolean connexion() {
        SQLiteDatabase db = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        String username = this.txtUsername.getText().toString();
        String password = this.txtMdp.getText().toString();

        clearInfo();

        if (!(username.length() > 3 && username.length() < 18)) {
            ajouterInfo("La longueur du pseudo est trop courte ou trop grande.");
            return false;
        }

        if ((password.length() < 3)) {
            ajouterInfo("La longueur du mot de passe est trop courte.");
            return false;
        }

        // execute SQL query
        Cursor cur;
        try {
            cur = db.rawQuery("SELECT * FROM User WHERE login='" + username + "'", null);
        } catch(SQLiteException e) {
            Log.d("dbQuery", "SQL error: " + e);
            return false;
        }

        if(cur.moveToFirst()) {
            if (username.equals(cur.getString(1)) && password.equals(cur.getString(2))) {
                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(intent);
                return false;
            }
            else {
                clearInfo();
                ajouterInfo("Utilisateur non trouvé !");
            }
           this.lblInfo.setText("Le compte n'a pas été trouvé.");
        }
        return true;
    }

    public void ajouterInfo(String text) {
        this.lblInfo.setText(this.lblInfo.getText() + text + "\n");
    }

    public void clearInfo() {
        this.lblInfo.setText("");
    }


}
